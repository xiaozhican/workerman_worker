/*
Navicat MySQL Data Transfer

Source Server         : Laragon
Source Server Version : 50724
Source Host           : localhost:3306
Source Database       : laravel56

Target Server Type    : MYSQL
Target Server Version : 50724
File Encoding         : 65001

Date: 2020-03-04 17:26:04
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for messages
-- ----------------------------
DROP TABLE IF EXISTS `messages`;
CREATE TABLE `messages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `room_id` int(11) DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of messages
-- ----------------------------
INSERT INTO `messages` VALUES ('1', '3', null, '你好', '2020-02-26 07:18:48', '2020-02-26 07:18:48');
INSERT INTO `messages` VALUES ('2', '1', null, '你吃了吗', '2020-02-26 07:19:09', '2020-02-26 07:19:09');
INSERT INTO `messages` VALUES ('3', '1', null, '你好', '2020-02-26 07:20:24', '2020-02-26 07:20:24');
INSERT INTO `messages` VALUES ('4', '3', null, '你们好', '2020-02-26 07:20:31', '2020-02-26 07:20:31');
INSERT INTO `messages` VALUES ('5', '3', null, '吃了吗', '2020-02-26 07:20:36', '2020-02-26 07:20:36');
INSERT INTO `messages` VALUES ('6', '1', null, '我吃了，你呢', '2020-02-26 07:20:49', '2020-02-26 07:20:49');
INSERT INTO `messages` VALUES ('7', '3', null, '我也吃了', '2020-02-26 07:20:55', '2020-02-26 07:20:55');
INSERT INTO `messages` VALUES ('8', '3', null, '今天好热', '2020-02-26 07:25:40', '2020-02-26 07:25:40');
INSERT INTO `messages` VALUES ('9', '3', null, '大家好', '2020-02-26 07:38:40', '2020-02-26 07:38:40');
INSERT INTO `messages` VALUES ('10', '3', '2', '干的漂亮', '2020-02-26 07:44:08', '2020-02-26 07:44:08');

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('1', '2014_10_12_000000_create_users_table', '1');
INSERT INTO `migrations` VALUES ('2', '2014_10_12_100000_create_password_resets_table', '1');
INSERT INTO `migrations` VALUES ('3', '2020_02_26_062408_create_messages_table', '2');

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'artisan', 'artisan@artisan.com', '$2y$10$JiGaQGKMPk5HHtMqDRFSs.eBovVHNmauMmBrj1BUTsdICyN9mfaaS', 'JFZgNSOZPrxhzS7dkBobYyQ5Zc0nFuuVdHVctcN7tt1yAq6ybEUKhzP0ZGdE', '2020-02-26 06:21:51', '2020-02-26 06:21:51');
INSERT INTO `users` VALUES ('2', 'pip', 'pip@pip.com', '$2y$10$nSwqAHXmqC.jL0OtGoxPP.rLM6Pa4pp4G8PCD8/5sYQMuR1TJWmLS', '3CgnkQYac6PZggnLwfd65gb5XzimE1KutLsxHuYsUu3dyzZQf5GXeNes7bOw', '2020-02-26 06:22:24', '2020-02-26 06:22:24');
INSERT INTO `users` VALUES ('3', 'ruby', 'ruby@ruby.com', '$2y$10$0w3sv5NifjDuG1rFRCFJ4.vsB.IvdMfmIH4jLrxmRaydL5.FY6oxC', 'Tf2hHW7Cz2y9XCkmJMN0OX9jErsFQceHWfvMGwffQPlnP8eXwJsBLIlSK75E', '2020-02-26 06:22:47', '2020-02-26 06:22:47');
